package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Request structure
type Request struct{}

// Response type
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// Handler for the API server
type Handler struct{}

// ServeHTTP handles what to do with the request
func (h *Handler) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// Set headers
	res.Header().Set("Content-Type", "application/json")

	// Handle options requests
	if req.Method == "OPTIONS" {
		// Set headers
		res.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, PATCH, DELETE")
		res.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")

		res.Write([]byte(`{"status": "success"}`))
		return
	}

	// Get the request body
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(res, err.Error(), 500)
		return
	}

	// Unpack the body if it exists
	var request Request
	if len(body) > 0 {
		fmt.Println("upacking the body")
		err := json.Unmarshal(body, &request)
		if err != nil {
			http.Error(res, err.Error(), 500)
			return
		}
	}

	// Define the response
	var response Response

	switch req.URL.Path {
	case "/ping":
		response.Status = "success"
		response.Message = "pong"
	default:
		http.Error(res, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	// Pack the response
	responseBytes, _ := json.Marshal(response)

	// Return the response
	res.Write(responseBytes)
	return
}

func main() {

	// Loop until settings have loaded
	for !Settings.Load() {
		time.Sleep(5000 * time.Millisecond)
	}

	// Start the api
	if err := http.ListenAndServe(":"+Settings.Server.Port, new(Handler)); err != nil {
		fmt.Println("HTTP Api failed, reason: " + err.Error() + "\n")
	}
}
