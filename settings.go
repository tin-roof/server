package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// API settings structure
type settings struct {
	Server struct {
		Port string `json:"Port"`
	} `json:"Server"`
}

// Location of the settings file
var settingsFile = "./settings.json"

// Settings for the API
var Settings settings

// Load settings
func (sets *settings) Load() bool {

	// Read the settings file
	bytes, err := ioutil.ReadFile(settingsFile)
	if err != nil {
		fmt.Println("Failed to read settings file %s: %v\n", settingsFile, err)
		return false
	}

	// Unpack the settings
	if err := json.Unmarshal(bytes, sets); err != nil {
		fmt.Println("Error reading settings file: %s\n", settingsFile)
		return false
	}

	return true
}
